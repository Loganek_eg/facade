package zadanie1;

import lombok.Getter;
import zadanie1.frother.MilkFrother;
import zadanie1.grinder.CoffeGrinder;
import zadanie1.grinder.CoffeePowderIngredient;

@Getter
public class AdvancedCoffeeMachine {
    public static final AdvancedCoffeeMachine INSTANCE = new AdvancedCoffeeMachine();
    CoffeBeverage coffeBeverage = new CoffeBeverage();
    CoffeGrinder grinder = new CoffeGrinder();
    CoffeBean coffeBean = new CoffeBean();

    MilkIngredient milkIngredient = new MilkIngredient();
    MilkFrother milkFrother = new MilkFrother();
    CoffeePowderIngredient coffeePowderIngredient = null;


    private AdvancedCoffeeMachine() {
    }

    public void addIngredient(CoffeeIngredient coffeeIngredient) {
        coffeBeverage.ingredientsList.add(coffeeIngredient);
    }


    public CoffeBeverage makeCoffeeBeverage() {
        CoffeBeverage coffe = coffeBeverage;
        coffeBeverage = new CoffeBeverage();
        return (coffe);
    }

}
