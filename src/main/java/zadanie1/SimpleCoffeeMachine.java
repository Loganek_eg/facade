package zadanie1;

import zadanie1.frother.MilkFrother;
import zadanie1.frother.MilkFrotherException;
import zadanie1.grinder.CoffeGrinder;
import zadanie1.grinder.CoffeePowderIngredient;
import zadanie1.grinder.CoffieGrinderException;

public class SimpleCoffeeMachine {
    private CoffeBeverage coffeBeverage = new CoffeBeverage();
    private CoffeGrinder grinder = new CoffeGrinder();
    private CoffeBean coffeBean = new CoffeBean();

    private MilkIngredient milkIngredient = new MilkIngredient();
    private MilkFrother milkFrother = new MilkFrother();
    private CoffeePowderIngredient coffeePowderIngredient = null;

    public void addIngredient(CoffeeIngredient coffeeIngredient) {
        coffeBeverage.ingredientsList.add(coffeeIngredient);
    }

    public CoffeBeverage makeEspresso() throws CoffieGrinderException {
        while (coffeePowderIngredient == null) {
            try {
                coffeePowderIngredient = grinder.grind(coffeBean);
            } catch (CoffieGrinderException cge) {
                System.out.println("Czyszcze smietniczek");
            }
        }
        addIngredient(new WaterIngredient(50));
        CoffeBeverage coffe = coffeBeverage;
        coffeBeverage = new CoffeBeverage();
        return (coffe);
    }

    public CoffeBeverage makeLatte() throws MilkFrotherException, CoffieGrinderException {
        while (coffeePowderIngredient == null) {
            try {
                coffeePowderIngredient = grinder.grind(coffeBean);
            } catch (CoffieGrinderException cge) {
                System.out.println("Czyszcze smietniczek");
            }
        }
        addIngredient(new WaterIngredient(100));
        addIngredient(new MilkFrother().froth(milkIngredient));
        CoffeBeverage coffe = coffeBeverage;
        coffeBeverage = new CoffeBeverage();
        return (coffe);
    }

    public CoffeBeverage makeCapuccino() throws MilkFrotherException, CoffieGrinderException {
        while (coffeePowderIngredient == null) {
            try {
                coffeePowderIngredient = grinder.grind(coffeBean);
            } catch (CoffieGrinderException cge) {
                System.out.println("Czyszcze smietniczek");
            }
        }
        addIngredient(new WaterIngredient(30));
        addIngredient(new MilkFrother().froth(milkIngredient));
        addIngredient(new MilkIngredient());
        CoffeBeverage coffe = coffeBeverage;
        coffeBeverage = new CoffeBeverage();
        return (coffe);
    }

    public CoffeBeverage makeAmericana() throws CoffieGrinderException {
        while (coffeePowderIngredient == null) {
            try {
                coffeePowderIngredient = grinder.grind(coffeBean);
            } catch (CoffieGrinderException cge) {
                System.out.println("Czyszcze smietniczek");
            }
        }
        addIngredient(new WaterIngredient(30));
        CoffeBeverage coffe = coffeBeverage;
        coffeBeverage = new CoffeBeverage();
        return (coffe);

    }

}
