package zadanie1;

import java.util.LinkedList;
import java.util.List;

public class CoffeBeverage {

    List<CoffeeIngredient> ingredientsList = new LinkedList<>();

    public CoffeBeverage(List<CoffeeIngredient> ingredientsList) {
        this.ingredientsList = ingredientsList;
    }

    public CoffeBeverage() {

    }


    @Override
    public String toString() {
        return "CoffeBeverage{" +
                 "" + ingredientsList +
                '}';
    }

}


