package zadanie1.grinder;

import zadanie1.CoffeeIngredient;

public class CoffeePowderIngredient extends CoffeeIngredient {

    CoffeePowderIngredient() {
    }

    @Override
    public String toString() {
        return "CoffeePowderIngredient{}";
    }
}
