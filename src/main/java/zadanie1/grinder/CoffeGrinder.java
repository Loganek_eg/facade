package zadanie1.grinder;

import zadanie1.CoffeBean;

public class CoffeGrinder {

    private int wasteContainerCount;

    public void empty() {
        wasteContainerCount = 0;
    }

    public CoffeePowderIngredient grind(CoffeBean coffeBean) throws CoffieGrinderException {
        if (wasteContainerCount < 3) {
           ++wasteContainerCount;
            System.out.println(wasteContainerCount);
        } else if (wasteContainerCount == 3) {
            empty();
        } else {
            throw new CoffieGrinderException();
        }
        return new CoffeePowderIngredient();

    }

    @Override
    public String toString() {
        return "CoffeGrinder{" +
                "wasteContainerCount=" + wasteContainerCount +
                '}';
    }
}
