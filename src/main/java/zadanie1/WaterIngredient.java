package zadanie1;

public class WaterIngredient extends CoffeeIngredient {

    private int waterInMl;

    public WaterIngredient(int waterInMl) {
        this.waterInMl = waterInMl;
    }

    @Override
    public String toString() {
        return "WaterIngredient{" +
                "waterInMl=" + waterInMl +
                '}';
    }
}
