package zadanie1.frother;

import zadanie1.CoffeeIngredient;

public class FoamedMilkIngredient extends CoffeeIngredient {
    FoamedMilkIngredient() {
    }

    @Override
    public String toString() {
        return "FoamedMilkIngredient{}";
    }
}
