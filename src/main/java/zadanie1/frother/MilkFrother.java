package zadanie1.frother;

import zadanie1.MilkIngredient;

public class MilkFrother {


    public FoamedMilkIngredient froth(MilkIngredient milkIngredient) throws MilkFrotherException {
        if (Math.random() < 0.08) throw new MilkFrotherException();
        else return new FoamedMilkIngredient();
    }

    @Override
    public String toString() {
        return "MilkFrother";
    }
}