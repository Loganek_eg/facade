package zadanie1;

import zadanie1.frother.MilkFrotherException;
import zadanie1.grinder.CoffieGrinderException;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws MilkFrotherException, CoffieGrinderException {
        SimpleCoffeeMachine simpleCoffeeMachine = new SimpleCoffeeMachine();
        System.out.println(simpleCoffeeMachine.makeEspresso());
        System.out.println(simpleCoffeeMachine.makeLatte());
        System.out.println(simpleCoffeeMachine.makeCapuccino());
        System.out.println(simpleCoffeeMachine.makeAmericana());
        System.out.println(simpleCoffeeMachine.makeEspresso());
        System.out.println(simpleCoffeeMachine.makeLatte());
        System.out.println(simpleCoffeeMachine.makeCapuccino());
        System.out.println(simpleCoffeeMachine.makeAmericana());
//
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Witaj, jestem coffemakerem powiedz jaką kawę chcesz");
//        String kawa = sc.nextLine();
//        System.out.println("wybrałeś kawę " + kawa);
//        System.out.println("Czy chcesz dodać cukru? y/n");
//        String cukier = sc.nextLine();
//        if (cukier.equals("y")) {
//            System.out.println("Dodałem cukier");
//            AdvancedCoffeeMachine.INSTANCE.addIngredient(new SugarIngredient());
//        } else if (cukier.equals("n")) {
//            System.out.println();
//        }
//        simpleCoffeeMachine.makeEspresso();
//    }
    }
}
